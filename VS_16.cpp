﻿#include <iostream>
#include <ctime>

int main()
{    
    //int iday;
    //std::cout << "Enter today's date ";
    //std::cin >> iday;
    //std::cout << "\n";

    time_t now = time(0);
    struct tm timeinfo;
    localtime_s(&timeinfo, &now);
    int iday = timeinfo.tm_mday;
    std::cout << iday << "\n" << std::endl;

    const int N = 5;

    int index = iday % N;

    int sum = 0;

    int array[N][N];
    for (int i = 0; i < N; i++)
    {        
        for (int j = 0; j < N; j++) 
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
            
            if(i==index)
                sum += array[index][j];            
        }

        std::cout << "\n";

    }  

    std::cout << "\n Sum of elements " << sum;
}